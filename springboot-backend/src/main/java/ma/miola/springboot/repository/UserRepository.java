package ma.miola.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.miola.springboot.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
